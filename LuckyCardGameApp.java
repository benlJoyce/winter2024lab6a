
import java.util.Scanner;
public class LuckyCardGameApp{

	public static void main(String[] args){
		//create deck object
		Deck deck = new Deck();
		deck.shuffle();
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to Lucky Card Game!");
		
		System.out.println("How many cards would you like to remove");
		int cardsToRemove = scanner.nextInt();
		
		System.out.println("The initial number of cards is: " + deck.length());
		for(int i = 0; i < cardsToRemove; i++){
			deck.drawTopCard();
		}
		System.out.println("The final number of cards is: " + deck.length());
		
		deck.shuffle();
		System.out.println("Shuffled deck: " + deck.toString());
	}
}
